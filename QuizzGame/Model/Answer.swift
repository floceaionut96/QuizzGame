//
//  Answer.swift
//  QuizzGame
//
//  Created by Flocea Ionut on 05.02.2022.
//

import Foundation

struct Answer {
    let text: String
    let correct: Bool
    
    init(text: String, correct: Bool) {
        self.text=text
        self.correct=correct
    }
}
