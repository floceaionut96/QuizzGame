//
//  QuizGame.swift
//  QuizzGame
//
//  Created by Flocea Ionut on 05.02.2022.
//

import Foundation

struct QuizGame {
    
    private(set) var questionNumber : Int
    
//    struct Raspuns {
//        let text: String
//        let correct: Bool
//    }
//
//    var raspunsulCurent: [Raspuns] {
//        get {
//            return currentAnswer.compactMap { answer in
//                return Raspuns(text: answer.text, correct: answer.correct)
//            }
//        }
//    }
    
    var currentAnswer : [Answer]
    
    var questions = [
        Question (text: "In which part of your body would you find the cruciate ligament?", answers: [Answer(text: "Knee", correct: true),Answer(text: "Ankle", correct: false),Answer(text: "Hand", correct: false)]),
        Question(text: "In what US State is the city Nashville?", answers: [Answer(text: "Nevada", correct: false),Answer(text: "Tennessee", correct: true),Answer(text: "Texas", correct: false)]),
        Question(text: "In which European country would you find the Rijksmuseum?", answers: [Answer(text: "Netherlands", correct: true),Answer(text: "Belgium", correct: false),Answer(text: "Germany", correct: false)]),
        Question(text: "How many human players are there on each side in a polo match?", answers: [Answer(text: "Six", correct: false),Answer(text: "Five", correct: false),Answer(text: "Four", correct: true)]),
        Question(text: "What band was Harry Styles in before his solo career?", answers: [Answer(text: "One Direction", correct: true),Answer(text: "Backstreet Boys", correct: false),Answer(text: "Linkin Park", correct: false)]),
        Question(text: "What sport did David Beckham play?", answers: [Answer(text: "Basketball", correct: false),Answer(text: "Football", correct: true),Answer(text: "Tenis", correct: false)]),
        Question(text: "How many permanent teeth does a dog have?", answers: [Answer(text: "42", correct: true),Answer(text: "38", correct: false),Answer(text: "36", correct: false)]),
        Question(text: "What’s longer, a nautical mile or a mile?", answers: [Answer(text: "A nautical mile", correct: true),Answer(text: "A mile", correct: false),Answer(text: "They are equal", correct: false)]),
        Question(text: "In what year did The Beatles split up?", answers: [Answer(text: "1986", correct: false),Answer(text: "1994", correct: false),Answer(text: "1970", correct: true)]),
        Question(text: "In what decade was pop icon Madonna born?", answers: [Answer(text: "1950s", correct: true),Answer(text: "1960s", correct: false),Answer(text: "1970s", correct: false)]),
        Question(text: "Which European city hosted the 1936 Summer Olympics?", answers: [Answer(text: "Berlin", correct: true),Answer(text: "London", correct: false),Answer(text: "Paris", correct: false)]),
        Question(text: "When did Big Brother first air on Channel 4?", answers: [Answer(text: "2004", correct: false),Answer(text: "2000", correct: true),Answer(text: "2002", correct: false)]),
        Question(text: "A screwdriver cocktail is orange juice, ice and which spirit?", answers: [Answer(text: "Whisky", correct: false),Answer(text: "Rum", correct: false),Answer(text: "Vodka", correct: true)]),
        Question(text: "Which southern Italian city is usually credited as the birthplace of the pizza?", answers: [Answer(text: "Palermo", correct: false),Answer(text: "Salerna", correct: false),Answer(text: "Naples", correct: true)]),
        Question(text: "What language is spoken in Brazil?", answers: [Answer(text: "Spanish", correct: false),Answer(text: "Portuguese", correct: true),Answer(text: "Italian", correct: false)]),
        Question(text: "How many countries are there in the region of Europe? (Recognised by the United Nations)", answers: [Answer(text: "44", correct: true),Answer(text: "36", correct: false),Answer(text: "32", correct: false)]),
        Question(text: "How many notes are there in a musical scale?", answers: [Answer(text: "Seven", correct: true),Answer(text: "Eight", correct: false),Answer(text: "Nine", correct: false)]),
        Question(text: "Glossectomy is the removal of all of or part of which body part?", answers: [Answer(text: "The tongue", correct: true),Answer(text: "The finger", correct: false),Answer(text: "The nose", correct: false)]),
        Question(text: "Which planet has the most moons?", answers: [Answer(text: "Jupiter", correct: false),Answer(text: "Saturn", correct: true),Answer(text: "Uranus", correct: false)]),
        Question(text: "What part of a plant conducts photosynthesis?", answers: [Answer(text: "Leaf", correct: true),Answer(text: "Root", correct: false),Answer(text: "Stem", correct: false)]),
        Question(text: "How many elements are in the periodic table?", answers: [Answer(text: "118", correct: true),Answer(text: "104", correct: false),Answer(text: "126", correct: false)]),
        Question(text: "How many hearts does an octopus have?", answers: [Answer(text: "One", correct: false),Answer(text: "Two", correct: false),Answer(text: "Three", correct: true)]),
        Question(text: "Where is the smallest bone in the human body located?", answers: [Answer(text: "Nose", correct: false),Answer(text: "Ear", correct: true),Answer(text: "Neck", correct: false)]),
        Question(text: "With over 222 million units sold, what is Apple's highest-selling iPhone model?", answers: [Answer(text: "iPhone 6/6 Plus", correct: true),Answer(text: "Iphone 3g", correct: false),Answer(text: "Iphone X", correct: false)]),
        Question(text: "In which year was the Microsoft XP operating system released?", answers: [Answer(text: "2000", correct: false),Answer(text: "2001", correct: true),Answer(text: "2002", correct: false)]),
        Question(text: "What is the longest Harry Potter film? (Non-extended versions)", answers: [Answer(text: "Harry Potter and the Philosopher's Stone", correct: false),Answer(text: "Harry Potter and the Order of the Phoenix", correct: false),Answer(text: "Harry Potter and the Chamber of Secrets", correct: true)]),
        Question(text: "What is the effect of the Obliviate spell in Harry Potter?", answers: [Answer(text: "Invisibility", correct: false),Answer(text: "Makes you fly", correct: false),Answer(text: "Removes memories", correct: true)])
    ]
    
    init() {
        self.questionNumber = Int.random(in:     0..<questions.count)
        self.currentAnswer = questions[questionNumber].answers
    }
    
    func getQuestionNumber() -> Int{
        return questionNumber
    }
    
    func getQuestions() ->[Question]{
        return questions
    }
    
    func size() -> Int{
        return questions.count
    }
    
    mutating func setQuestionNumber() {
        questions.remove(at: questionNumber)
        questionNumber = Int.random(in:     0..<questions.count)
    }
    
    func getQuestionText() -> String {
        return questions[questionNumber].text
    }
    
    mutating func shuffleAnswers() {
       currentAnswer=questions[questionNumber].answers
       currentAnswer.shuffle()
    }
    
    
    mutating func getQuestionAnswers() -> [Answer]{
        shuffleAnswers()
        return currentAnswer
    }
    
    func trueAnswer() -> Int{
        for (index, answer) in currentAnswer.enumerated() {
            if answer.correct == true {
                return index
            }
        }
        return 3
    }
    
    
}
