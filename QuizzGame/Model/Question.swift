//
//  Question.swift
//  QuizzGame
//
//  Created by Flocea Ionut on 05.02.2022.
//

import Foundation

struct Question {
    let text: String
    let answers: [Answer]
    
    init(text: String, answers: [Answer]) {
        self.text=text
        self.answers=answers
    }
}
