//
//  ViewController.swift
//  QuizzGame
//
//  Created by Flocea Ionut on 05.02.2022.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var ans1Button: UIButton!
    
    @IBOutlet weak var ans2Button: UIButton!
    
    @IBOutlet weak var ans3Button: UIButton!
    
    @IBOutlet weak var progressBarView: UIProgressView!
    
    var quizGame = QuizGame()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        setButtonsColor()
        progressBarView.progress = 0.1
        
    }
    
    @IBAction func answerButtonPressed(_ sender: UIButton) {
        
        if progressBarView.progress == 1 {
            let secondVC = SecondViewController()
            self.present(secondVC,animated: true,completion: nil)
        }
        
        let correctIndex = quizGame.trueAnswer()
        
        if sender.tag == correctIndex{
            sender.backgroundColor = UIColor.green
        } else {
            sender.backgroundColor = UIColor.red
        }
        
        Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(setButtonsColor), userInfo: nil, repeats: false)
    
        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(updateUI), userInfo: nil, repeats: false)
        
        
    }
    
    @objc func setButtonsColor(){
        let buttonColor = UIColor(named: "#FFC947")
        
        ans1Button.backgroundColor = buttonColor
        ans2Button.backgroundColor = buttonColor
        ans3Button.backgroundColor = buttonColor
        
    }
    
    
    @objc func updateUI(){
        
        let answers = quizGame.getQuestionAnswers()
        
        questionLabel.text = quizGame.getQuestionText()
        
        ans1Button.setTitle(answers[0].text, for: .normal)
        ans2Button.setTitle(answers[1].text, for: .normal)
        ans3Button.setTitle(answers[2].text, for: .normal)
        
        quizGame.setQuestionNumber()
        progressBarUpgrade()
        
    }
    
    func progressBarUpgrade(){
        progressBarView.progress += 0.1
    }

}
